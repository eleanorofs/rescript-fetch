@val external window: Dom.window = "window";

/* Fetch with a string */

let _: Promise.t<Response.t> =
  Fetch.withString("https://jsonplaceholder.typicode.com/users");

/* or create a request */

let _: Promise.t<Response.t> =
  Request.Make.fromUrl("https://jsonplaceholder.typicode.com/users")
  -> Fetch.withRequest;

/* or for more control, use request options with the convenient default.*/

let options: Options.t = {...Options.default,
                          method: Some(#POST),
                          mode: Some(#"same-origin")
                         };
let _: Request.t =
  "https://jsonplaceholder.typicode.com/users"
  -> Request.Make.WithOptions.fromUrl(options); 

/* and handle Responses with the appropriate properties and methods */

let _: Promise.t<Js.Json.t> =
  Fetch.withString("https://jsonplaceholder.typicode.com/users")
  -> Promise.thenResolve(response => Response.clone(response))
  -> Promise.thenResolve(response => Response.json(response));
