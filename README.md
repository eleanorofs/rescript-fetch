# rescript-fetch

a binding for the [Fetch API](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API). 

## reason for being

It's not easy to justify writing this package when the 
venerable [`bs-fetch`](https://www.npmjs.com/package/bs-fetch) still exists 
and works. However, ultimately going forward, I want a fetch binding that meets
the following goals: 

* The new, preferred binding for ReScript promises is the
[`rescript-promise`](https://rescript-lang.org/docs/manual/latest/promise) 
package. If I'm writing greenfield code, then that's the binding I want to use.
* Honestly, I really struggle to read proper OCaml anymore, and I'm sure that
goes double for people who never cut their teeth on ReasonML. 

## breaking changes

My philosophy on breaking changes is that they are inevitable. I try to be 
nice with semantic versioning and just not go "1.0.0" but if you can't tolerate
some light refactoring after an update, you shouldn't add this dependency. 

I don't anticipate breaking changes in the underlying Fetch API, but when 
`rescript-promise` starts getting shipped with the main ReScript, I do intend
to upgrade the promises returned in this package. 

## Contributing

Please feel free! Open issues or add any missing bindings you find. Do try
to follow the style of the existing code. 

Since this is just a binding and doesn't have any particular logic on its own, 
I tend to think this is fairly self-documenting, but feel free to open issues
if you have any questions on how the existing code works. (Since there are no
highly experienced ReScript developers right now, so there are no stupid
questions.)


