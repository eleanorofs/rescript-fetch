@val external withString: string => Promise.t<Response.t> = "fetch";

@val external withRequest: Request.t => Promise.t<Response.t> = "fetch";

/* HACK using an "init" seems redundant with Request. I kind of like having one
   way to do things. Open to issues though. */
