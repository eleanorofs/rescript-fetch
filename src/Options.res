type t = {
  method: option<Method.t>,
  headers: option<Js.Dict.t<string>>, /* TODO consider a Header type. */
  body: option<string>,
  mode: option<Mode.t>,
  credentials: option<Credentials.t>,
  cache: option<Cache.t>,
  redirect: option<Redirect.t>,
  referrer: option<string>,
  integrity: option<string>
};

let default: t = {
  method: None,
  headers: None, 
  body: None,
  mode: None,
  credentials: None,
  cache: None,
  redirect: None,
  referrer: None,
  integrity: None
};
