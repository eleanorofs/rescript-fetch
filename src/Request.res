type t;

module Make = {
  @new external fromUrl: string => t = "Request";
  @new external fromRequest: t => t = "Request";

  module WithOptions = {
    @new external fromUrl: (string, Options.t) => t = "Request";
    @new external fromRequest: (t, Options.t) => t = "Request";
  };
};
