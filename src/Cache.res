type t = [ #default | #"no-store" | #reload | #"no-cache"
         | #"force-cache" | #"only-if-cached"];