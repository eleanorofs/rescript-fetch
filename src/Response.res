type t; 

/* TODO will anyone ever use the constructor? */

/* TODO how to handle ReadableStream body property? */

@get external bodyUsed: t => bool = "bodyUsed";

@get external ok: t => bool = "ok";

@get external redirected: t => bool = "redirected";

@get external status: t => int = "status";

@get external statusText: t => string = "statusText";

/* TODO trailers? */

@get external type_: t => ResponseType.t = "type";

@get external url: t => string = "url";

@send external arrayBuffer: t => Promise.t<Js.TypedArray2.ArrayBuffer.t>
  = "arrayBuffer";

/* TODO blob? */

@send external clone: t => t = "clone";

@send external error: t => t = "error";

/* TODO formdata? */

@send external json: t => Js.Json.t = "json";

@send external redirect: t => t = "redirect";

@send external text: t => Promise.t<string> = "text";
